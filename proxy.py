from aiohttp import web
import socketio
import asyncio
import uuid
import os
import json
import logging
from typing import Dict

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
PORT = int(os.getenv('HTTP_PORT', 9990))
REQUEST_TIMEOUT = int(os.getenv('REQUEST_TIMEOUT_SECONDS', 10))
SOCKET_IO_NAMESPACE = os.getenv('SOCKET_IO_NAMESPACE', None)
SOCKET_IO_ASK_EVENT = os.getenv('SOCKET_IO_ASK_EVENT', 'ask')
SOCKET_IO_ANSWER_EVENT = os.getenv('SOCKET_IO_ANSWER_EVENT', 'answer')

sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)

contexts: Dict[str, asyncio.Queue] = {}


async def proxy(request):
    # headers = json.dumps(request.headers)
    context_id: str = str(uuid.uuid4())
    queue: asyncio.Queue = asyncio.Queue()
    contexts[context_id] = queue
    logging.debug(request.text())
    # Now support only json
    data = {'id': context_id, 'data': await request.json()}
    await sio.emit(SOCKET_IO_ASK_EVENT, data)
    (status_code, response, failure) = (200, None, None)
    try:
        response = await asyncio.wait_for(queue.get(), timeout=REQUEST_TIMEOUT)  # seconds
    except asyncio.TimeoutError:
        (status_code, failure) = (400, 'Cant receive response from destination socket')
    finally:
        del contexts[context_id]
    return web.Response(status=status_code, text=response, reason=failure, content_type='application/json')


async def about(request):
    return web.Response(text=json.dumps({'REQUEST_TIMEOUT_SECONDS': REQUEST_TIMEOUT,
                                         'SOCKET_IO_NAMESPACE': SOCKET_IO_NAMESPACE,
                                         'SOCKET_IO_ASK_EVENT': SOCKET_IO_ASK_EVENT,
                                         'SOCKET_IO_ANSWER_EVENT': SOCKET_IO_ANSWER_EVENT}),
                        content_type='application/json')


async def healthcheck(request):
    return web.Response(text='ok', content_type='text/plain')


@sio.on('connect', namespace=SOCKET_IO_NAMESPACE)
def connect(sid, environ):
    logging.info('connect ' + str(sid))


@sio.on(SOCKET_IO_ANSWER_EVENT, namespace=SOCKET_IO_NAMESPACE)
async def answer(sid, response):
    logging.debug(response)
    data = json.loads(response)
    id = data['id']
    if id in contexts:
        await contexts[id].put(json.dumps(data['data']))


@sio.on('disconnect', namespace=SOCKET_IO_NAMESPACE)
def disconnect(sid):
    logging.info('disconnect ' + str(sid))


app.router.add_post('/proxy', proxy)
app.router.add_get('/about', about)
app.router.add_get('/healthcheck', healthcheck)

if __name__ == '__main__':
    web.run_app(app, port=PORT) 
