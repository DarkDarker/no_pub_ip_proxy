FROM python:3.7

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt

ENV HTTP_PORT=9990

HEALTHCHECK CMD curl --fail -s http://localhost:${HTTP_PORT}/healthcheck || exit 1

ENTRYPOINT  python3 proxy.py